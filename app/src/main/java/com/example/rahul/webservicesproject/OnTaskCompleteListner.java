package com.example.rahul.webservicesproject;

public interface OnTaskCompleteListner {
    void OnTaskCompleted(String results, String tag);
}
