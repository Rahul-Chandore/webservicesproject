package com.example.rahul.webservicesproject;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.rahul.webservicesproject.WebServices.Executor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnTaskCompleteListner {
    Context context = this;
    TextView txtShare;
    ListView list;
    ArrayList<Student> studentList;
    String url = "https://www.dropbox.com/s/9uuyt042bwi2una/StudentJson.txt?dl=1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.list);
        txtShare = findViewById(R.id.txtShare);
        txtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iShare = new Intent(Intent.ACTION_SEND);
                iShare.setType("text/plain");
                iShare.putExtra(Intent.EXTRA_SUBJECT, "SHARE_IT");
                String msg = "Its a Awesome app Download Now\n";
                msg = msg + "https://play.google.com\n\n";
                iShare.putExtra(Intent.EXTRA_TEXT, msg);
                startActivity(Intent.createChooser(iShare, "Select One"));

            }
        });

        callStudentws();
    }

    private void callStudentws() {
        Executor executor = new Executor(context, "StudentWs");
        executor.execute(url);
    }

    @Override
    public void OnTaskCompleted(String results, String tag) {
        if (tag.equals("StudentWs")) {
            studentList = new ArrayList<>();
            try {
                JSONObject mainobj = new JSONObject(results);
                JSONArray studentArray = mainobj.getJSONArray("student");
                for (int i = 0; i < studentArray.length(); i++) {
                    JSONObject jsonObject = (JSONObject) studentArray.get(i);
                    int id = jsonObject.getInt("id");
                    String name = jsonObject.getString("name");
                    String city = jsonObject.getString("city");
                    String Gender = jsonObject.getString("Gender");
                    int age = jsonObject.getInt("age");
                    String birthdate = jsonObject.getString("birthdate");

                    Student s = new Student();
                    s.setId(id);
                    s.setName(name);
                    s.setAge(age);
                    s.setCity(city);
                    s.setGender(Gender);
                    s.setBirthdate(birthdate);

                    studentList.add(s);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            StudentAdapter sd = new StudentAdapter(context, studentList);
            list.setAdapter(sd);
        }
    }
}
